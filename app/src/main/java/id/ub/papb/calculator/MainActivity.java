package id.ub.papb.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button bt1,bt2,bt3,bt4,bt5,bt6,bt7,bt8,bt9,bt0;
    Button bttambah,btkurang,btkali,btbagi,btkoma,bthasil,btclear;
    TextView operasi;
    float bil1, bil2;
    boolean tambah, kurang, kali, bagi;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        bt3 = findViewById(R.id.bt3);
        bt4 = findViewById(R.id.bt4);
        bt5 = findViewById(R.id.bt5);
        bt6 = findViewById(R.id.bt6);
        bt7 = findViewById(R.id.bt7);
        bt8 = findViewById(R.id.bt8);
        bt9 = findViewById(R.id.bt9);
        bt0 = findViewById(R.id.bt0);
        bttambah = findViewById(R.id.bttambah);
        btkurang = findViewById(R.id.btkurang);
        btkali = findViewById(R.id.btkali);
        btbagi = findViewById(R.id.btbagi);
        btkoma = findViewById(R.id.btkoma);
        bthasil = findViewById(R.id.bthasil);
        btclear = findViewById(R.id.btclear);
        operasi = findViewById(R.id.operasi);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "1");
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "2");
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "3");
            }
        });
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "4");
            }
        });
        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "5");
            }
        });
        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() +"6");
            }
        });
        bt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() +"7");
            }
        });
        bt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() +"8");
            }
        });
        bt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "9");
            }
        });
        bt0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "0");
            }
        });
        bttambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (operasi == null) {
                    operasi.setText("");
                } else {
                    bil1 = Float.parseFloat(operasi.getText() + "");
                    tambah = true;
                    operasi.setText(null);
                }
            }
        });
        btkurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil1 = Float.parseFloat(operasi.getText() + "");
                kurang= true;
                operasi.setText(null);
            }
        });
        btkali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil1 = Float.parseFloat(operasi.getText() + "");
                kali = true;
                operasi.setText(null);
            }
        });
        btbagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil1 = Float.parseFloat(operasi.getText() + "");
                bagi = true;
                operasi.setText(null);
            }
        });
        btkoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + ".");
            }
        });
        btclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText("");
            }
        });
        bthasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil2 = Float.parseFloat(operasi.getText() + "");
                String hitung = "";
                if (tambah == true) {
                    operasi.setText(bil1 + bil2 + "");
                    hitung = bil1 + " + " + bil2;
                    tambah = false;
                }
                if (kurang == true) {
                    operasi.setText(bil1 - bil2 + "");
                    hitung = bil1 + " - " + bil2;
                    kurang = false;
                }
                if (kali == true) {
                    operasi.setText(bil1 * bil2 + "");
                    hitung = bil1 + " * " + bil2;
                    kali = false;
                }
                if (bagi == true) {
                    operasi.setText(bil1 / bil2 + "");
                    hitung = bil1 + " / " + bil2;
                    bagi = false;
                }
                Intent intent =new Intent (context,MainActivity2.class);
                intent.putExtra("Operasi",operasi.getText().toString());
                intent.putExtra("Hitung", hitung);
                startActivity(intent);
            }
        });
    }
}