package id.ub.papb.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity2 extends AppCompatActivity {
    TextView operasihitung,hasiloperasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        operasihitung = findViewById(R.id.operasihitung);
        hasiloperasi = findViewById(R.id.hasiloperasi);

        Intent intent = getIntent();
        String hasil = intent.getStringExtra("Hitung");
        String operasi = intent.getStringExtra("Operasi");

        operasihitung.setText(hasil);
        hasiloperasi.setText(operasi);
    }
}